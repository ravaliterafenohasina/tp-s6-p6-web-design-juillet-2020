<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('textareas', 'TextareaController');

Route::resource('listareas', 'ListareaController');

Route::resource('titleh2s', 'Titleh2Controller');

Route::resource('titleh1s', 'Titleh1Controller');

Route::resource('imgareas', 'ImgareaController');

Route::resource('titles', 'TitleController');
Route::resource('admin', 'TitleController');
