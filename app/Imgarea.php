<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imgarea extends Model
{
    protected $fillable = ['content', 'alt', 'idparent'];
}
