<?php

namespace App\Http\Controllers;

use App\Imgarea;
use Illuminate\Http\Request;

class ImgareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imgareas = Imgarea::all();
        return view('imgareas.index', compact('imgareas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('imgareas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'alt' => 'required',
            'idparent' => 'required'
        ]);

        Imgarea::create($request->all());

        return redirect()->route('titles.index')->with('success', 'Image added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Imgarea  $imgarea
     * @return \Illuminate\Http\Response
     */
    public function show(Imgarea $imgarea)
    {
        return view('imgareas.show', compact('imgarea'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Imgarea  $imgarea
     * @return \Illuminate\Http\Response
     */
    public function edit(Imgarea $imgarea)
    {
        return view('imgareas.edit', compact('imgarea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Imgarea  $imgarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Imgarea $imgarea)
    {
        $request->validate([
            'content' => 'required',
            'alt' => 'required'
        ]);

        $imgarea->update($request->all());

        return redirect()->route('titles.index')->with('success', 'Image updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Imgarea  $imgarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Imgarea $imgarea)
    {
        $imgarea->delete();
        return redirect()->route('titles.index')->with('success', 'Image deleted succesfully');
    }
}
