<?php

namespace App\Http\Controllers;

use App\Textarea;
use Illuminate\Http\Request;
use App\Titleh1;
use App\Titleh2;

class CliContoller extends Controller
{
    public function showH1()
    {
        $titles = Titleh1::all();
        return response()->json($titles, 200);
    }

    public function showH2($id)
    {
        $titles = Titleh2::where('idparent', '=', $id)->get();
        return response()->json($titles, 200);
    }

    public function showText($id)
    {
        $textes = Textarea::where('idparent', '=', $id)->get();
        return response()->json($textes, 200);
    }
}
