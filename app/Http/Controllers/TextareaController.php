<?php

namespace App\Http\Controllers;

use App\Textarea;
use App\Imgarea;
use Illuminate\Http\Request;

class TextareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $textareas = Textarea::all();
        return view('textareas.index', compact('textareas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('textareas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required',
            'idparent' => 'required'
        ]);

        Textarea::create($request->all());

        return redirect()->route('titles.index')->with('success', 'Text added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Textarea  $textarea
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $imgareas = Imgarea::where('idparent', '=', $id)->get();
        return view('imgareas.index', compact('imgareas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Textarea  $textarea
     * @return \Illuminate\Http\Response
     */
    public function edit(Textarea $textarea)
    {
        return view('textareas.edit', compact('textarea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Textarea  $textarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Textarea $textarea)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required'
        ]);

        $textarea->update($request->all());

        return redirect()->route('titles.index')->with('success', 'Text updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Textarea  $textarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Textarea $textarea)
    {
        $textarea->delete();
        return redirect()->route('titles.index')->with('success', 'Text deleted succesfully');
    }
}
