<?php

namespace App\Http\Controllers;

use App\Titleh1;
use App\Titleh2;
use Illuminate\Http\Request;

class Titleh1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titleh1s = Titleh1::all();
        return view('titleh1s.index', compact('titleh1s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('titleh1s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required',
            'idparent' => 'required'
        ]);

        Titleh1::create($request->all());

        return redirect()->route('titles.index')->with('success', 'H1 added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Titleh1  $titleh1
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $titleh2s = Titleh2::where('idparent', '=', $id)->get();
        return view('titleh2s.index', compact('titleh2s'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Titleh1  $titleh1
     * @return \Illuminate\Http\Response
     */
    public function edit(Titleh1 $titleh1)
    {
        return view('titleh1s.edit', compact('titleh1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Titleh1  $titleh1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Titleh1 $titleh1)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required'
        ]);

        $titleh1->update($request->all());

        return redirect()->route('titles.index')->with('success', 'H1 updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Titleh1  $titleh1
     * @return \Illuminate\Http\Response
     */
    public function destroy(Titleh1 $titleh1)
    {
        $titleh1->delete();
        return redirect()->route('titles.index')->with('success', 'H1 deleted succesfully');
    }
}
