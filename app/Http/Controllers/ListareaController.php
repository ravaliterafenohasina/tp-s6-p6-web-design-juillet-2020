<?php

namespace App\Http\Controllers;

use App\Listarea;
use Illuminate\Http\Request;

class ListareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listareas = Listarea::all();
        return view('listareas.index', compact('listareas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('listareas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required',
            'idparent' => 'required'
        ]);

        Listarea::create($request->all());

        return redirect()->route('listareas.index')->with('success', 'Text added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listarea  $listarea
     * @return \Illuminate\Http\Response
     */
    public function show(Listarea $listarea)
    {
        return view('listareas.show', compact('listarea'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listarea  $listarea
     * @return \Illuminate\Http\Response
     */
    public function edit(Listarea $listarea)
    {
        return view('listareas.edit', compact('listarea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listarea  $listarea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listarea $listarea)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required'
        ]);

        $listarea->update($request->all());

        return redirect()->route('listareas.index')->with('success', 'Text updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listarea  $listarea
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listarea $listarea)
    {
        $listarea->delete();
        return redirect()->route('listareas.index')->with('success', 'Text deleted succesfully');
    }
}
