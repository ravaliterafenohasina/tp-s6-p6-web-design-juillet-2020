<?php

namespace App\Http\Controllers;

use App\Titleh2;
use App\Textarea;

use Illuminate\Http\Request;

class Titleh2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titleh2s = Titleh2::all();
        return view('titleh2s.index', compact('titleh2s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('titleh2s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required',
            'idparent' => 'required'
        ]);

        Titleh2::create($request->all());

        return redirect()->route('titles.index')->with('success', 'H2 added sucessfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Titleh2  $titleh2
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $textareas = Textarea::where('idparent', '=', $id)->get();
        return view('textareas.index', compact('textareas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Titleh2  $titleh2
     * @return \Illuminate\Http\Response
     */
    public function edit(Titleh2 $titleh2)
    {
        return view('titleh2s.edit', compact('titleh2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Titleh2  $titleh2
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Titleh2 $titleh2)
    {
        $request->validate([
            'content' => 'required',
            'rank' => 'required'
        ]);

        $titleh2->update($request->all());

        return redirect()->route('titles.index')->with('success', 'H2 updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Titleh2  $titleh2
     * @return \Illuminate\Http\Response
     */
    public function destroy(Titleh2 $titleh2)
    {
        $titleh2->delete();
        return redirect()->route('titles.index')->with('success', 'H2 deleted succesfully');
    }
}
