<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listarea extends Model
{
    protected $fillable = ['content', 'rank', 'idparent'];
}
