<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titleh1 extends Model
{
    protected $fillable = ['content', 'rank', 'idparent'];
}
