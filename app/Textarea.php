<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Textarea extends Model
{
    protected $fillable = ['content', 'rank', 'idparent'];
}
