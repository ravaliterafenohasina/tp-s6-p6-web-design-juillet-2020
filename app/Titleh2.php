<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titleh2 extends Model
{
    protected $fillable = ['content', 'rank', 'idparent'];
}
