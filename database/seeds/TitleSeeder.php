<?php

use Illuminate\Database\Seeder;
use App\Title;

class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Title::create([
            'content' => "Leravel-Tutorial : Tutorial about CRUD",
            'description' => "This article will give you example of laravel crud example. This article goes in detailed
            on laravel tutorial for beginners. Step by step explain how to create crud in laravel .
            Here, Creating a basic example of laravel  crud application tutorial.",
        ]);
    }
}
