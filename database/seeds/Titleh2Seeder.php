<?php

use App\Titleh2;
use Illuminate\Database\Seeder;

class Titleh2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Titleh2::create([
            'content' => "Laravel - Database Configuration",
            'rank' => 1,
            'idparent' => "1"
        ]);

        Titleh2::create([
            'content' => "Laravel - Create Migration",
            'rank' => 2,
            'idparent' => "1"
        ]);

        Titleh2::create([
            'content' => "Laravel - Add Resource Route",
            'rank' => 3,
            'idparent' => "1"
        ]);

        Titleh2::create([
            'content' => "Laravel - Add Controller and Model",
            'rank' => 4,
            'idparent' => "1"
        ]);

        Titleh2::create([
            'content' => "Laravel - Add Blade Files",
            'rank' => 5,
            'idparent' => "1"
        ]);
    }
}
