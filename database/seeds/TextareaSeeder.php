<?php

use Illuminate\Database\Seeder;
use App\Textarea;

class TextareaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Textarea::create([
            'content' => "We will make database configuration for
            example database name, username, password etc for our crud application of laravel 7.
            So let s open .env file and fill all details like as bellow:",
            'rank' => 1,
            'idparent' => "1"
        ]);



        Textarea::create([
            'content' => "We are going to create crud
            application for product. so we have to create migration for 'products' table using
            Laravel 7 php artisan command, so first fire bellow command:",
            'rank' => 1,
            'idparent' => "2"
        ]);

        Textarea::create([
            'content' => "After this command you will
            find one file in following path `database/migrations` and you have to put bellow code
            in your migration file for create products table.",
            'rank' => 2,
            'idparent' => "2"
        ]);

        Textarea::create([
            'content' => "Now you have to run this migration by
            following command:",
            'rank' => 3,
            'idparent' => "2"
        ]);



        Textarea::create([
            'content' => "Here, we need to add resource route
            for product crud application. so open your 'routes/web.php' file and add following route.",
            'rank' => 1,
            'idparent' => "3"
        ]);


        Textarea::create([
            'content' => "In this step, now we should create new
            controller as ProductController. So run bellow command and create new controller.
            Bellow controller for create resource controller.",
            'rank' => 1,
            'idparent' => "4"
        ]);

        Textarea::create([
            'content' => "After bellow command you will find new
            file in this path 'App/Http/Controllers/ProductController.php'.
            In this controller will create seven methods by default as bellow methods:",
            'rank' => 2,
            'idparent' => "4"
        ]);

        Textarea::create([
            'content' => "So, let s copy bellow code and put on
            ProductController.php file.",
            'rank' => 3,
            'idparent' => "4"
        ]);

        Textarea::create([
            'content' => "Ok, so after run bellow command you will
            find 'app/Product.php' and put bellow content in Product.php file:",
            'rank' => 4,
            'idparent' => "4"
        ]);



        Textarea::create([
            'content' => "In last step. In this step we have to
    	    create just blade files. So mainly we have to create layout file and then create new folder
		    'products' then create blade files of crud app. So finally you have to create following
		    bellow blade file:",
            'rank' => 1,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "resources/views/products/layout.blade.php",
            'rank' => 2,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "resources/views/products/index.blade.php",
            'rank' => 3,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "resources/views/products/create.blade.php",
            'rank' => 4,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "resources/views/products/edit.blade.php",
            'rank' => 5,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "resources/views/products/show.blade.php",
            'rank' => 6,
            'idparent' => "5"
        ]);

        Textarea::create([
            'content' => "Now we are ready to run our crud application example with larave.",
            'rank' => 7,
            'idparent' => "5"
        ]);
    }
}
