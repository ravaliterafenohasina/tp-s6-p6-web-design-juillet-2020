<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(TitleSeeder::class);
        $this->call(Titleh1Seeder::class);
        $this->call(Titleh2Seeder::class);
        $this->call(TextareaSeeder::class);
        $this->call(ImgareaSeeder::class);
    }
}
