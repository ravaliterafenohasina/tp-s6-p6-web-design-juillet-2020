<?php

use Illuminate\Database\Seeder;
use App\Titleh1;

class Titleh1Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Titleh1::create([
            'content' => "Laravel-Tutorial | CRUD in Laravel For Beginners",
            'rank' => 1,
            'idparent' => "1"
        ]);
    }
}
