<?php

use Illuminate\Database\Seeder;
use App\Imgarea;

class ImgareaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Imgarea::create([
            'content' => "db-configuration.png",
            'alt' => "db-configuration",
            'idparent' => '1'
        ]);

        Imgarea::create([
            'content' => "create-migration-cmd.png",
            'alt' => "create-migration-cmd",
            'idparent' => '2'
        ]);

        Imgarea::create([
            'content' => "create-migration.png",
            'alt' => "create-migration",
            'idparent' => '3'
        ]);

        Imgarea::create([
            'content' => "migrate-cmd.png",
            'alt' => "migrate-cmd",
            'idparent' => '4'
        ]);

        Imgarea::create([
            'content' => "route.png",
            'alt' => "route",
            'idparent' => '5'
        ]);

        Imgarea::create([
            'content' => "controller-model-cmd.png",
            'alt' => "controller-model-cmd",
            'idparent' => '6'
        ]);

        Imgarea::create([
            'content' => "controller-model.png",
            'alt' => "controller-model",
            'idparent' => '7'
        ]);

        Imgarea::create([
            'content' => "product-model.png",
            'alt' => "product-model",
            'idparent' => '8'
        ]);

        Imgarea::create([
            'content' => "layout-blade-php.png",
            'alt' => "layout-blade-php",
            'idparent' => '9'
        ]);

        Imgarea::create([
            'content' => "index-blade-php.png",
            'alt' => "index-blade-php",
            'idparent' => '10'
        ]);

        Imgarea::create([
            'content' => "create-blade-php.png",
            'alt' => "create-blade-php",
            'idparent' => '11'
        ]);

        Imgarea::create([
            'content' => "edit-blade-php.png",
            'alt' => "edit-blade-php",
            'idparent' => '12'
        ]);

        Imgarea::create([
            'content' => "show-blade-php.png",
            'alt' => "show-blade-php",
            'idparent' => '13'
        ]);
    }
}
