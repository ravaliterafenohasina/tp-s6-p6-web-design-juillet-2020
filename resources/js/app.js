require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter);

import Home from './components/HomeComponent.vue';
import Tuto from './components/TutoComponent.vue';

Vue.component('title-area', require('./components/TitleComponent.vue').default);
Vue.component('text-area', require('./components/TextComponent.vue').default);


const routes = [{
        path: '/',
        component: Home
    },
    {
        path: '/tutos',
        component: Tuto
    }
];

const router = new VueRouter({
    routes
});



const app = new Vue({
    el: '#app',
    router: router
});
