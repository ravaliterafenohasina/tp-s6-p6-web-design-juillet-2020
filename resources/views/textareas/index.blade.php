@extends('textareas.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Text for - ID parent</h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('textareas.create') }}"> Create New Text</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Rank</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($textareas as $textarea)
    <tr>
        <td>{{ $textarea->id }}</td>
        <td>{{ $textarea->content }}</td>
        <td>{{ $textarea->rank }}</td>
        <td>{{ $textarea->created_at }}</td>
        <td>{{ $textarea->updated_at }}</td>

        <td>
            <form action="{{ route('textareas.destroy',$textarea->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('textareas.show',$textarea->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('textareas.edit',$textarea->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
