@extends('titleh2s.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>H2</h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('titleh2s.create') }}"> Create New Text</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Rank</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($titleh2s as $titleh2)
    <tr>
        <td>{{ $titleh2->id }}</td>
        <td>{{ $titleh2->content }}</td>
        <td>{{ $titleh2->rank }}</td>
        <td>{{ $titleh2->created_at }}</td>
        <td>{{ $titleh2->updated_at }}</td>

        <td>
            <form action="{{ route('titleh2s.destroy',$titleh2->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('titleh2s.show',$titleh2->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('titleh2s.edit',$titleh2->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
