@extends('textareas.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>List for - ID parent</h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('listareas.create') }}"> Create New Text</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Rank</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($listareas as $listarea)
    <tr>
        <td>{{ $listarea->id }}</td>
        <td>{{ $listarea->content }}</td>
        <td>{{ $listarea->rank }}</td>
        <td>{{ $listarea->created_at }}</td>
        <td>{{ $listarea->updated_at }}</td>

        <td>
            <form action="{{ route('listareas.destroy',$listarea->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('listareas.show',$listarea->id) }}">Show</a>

                <a class="btn btn-primary" style="margin:5px" href="{{ route('listareas.edit',$listarea->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
