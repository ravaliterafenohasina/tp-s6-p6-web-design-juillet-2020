@extends('titleh1s.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>H1</h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('titleh1s.create') }}"> Create New H1</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Rank</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($titleh1s as $titleh1)
    <tr>
        <td>{{ $titleh1->id }}</td>
        <td>{{ $titleh1->content }}</td>
        <td>{{ $titleh1->rank }}</td>
        <td>{{ $titleh1->created_at }}</td>
        <td>{{ $titleh1->updated_at }}</td>

        <td>
            <form action="{{ route('titleh1s.destroy',$titleh1->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('titleh1s.show',$titleh1->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('titleh1s.edit',$titleh1->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
