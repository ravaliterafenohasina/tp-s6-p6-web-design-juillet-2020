@extends('textareas.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Images</h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('imgareas.create') }}"> Create New Text</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Rank</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($imgareas as $imgarea)
    <tr>
        <td>{{ $imgarea->id }}</td>
        <td>{{ $imgarea->content }}</td>
        <td>{{ $imgarea->alt }}</td>
        <td>{{ $imgarea->created_at }}</td>
        <td>{{ $imgarea->updated_at }}</td>

        <td>
            <form action="{{ route('imgareas.destroy',$imgarea->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('imgareas.show',$imgarea->id) }}">Show</a>

                <a class="btn btn-primary" style="margin:5px" href="{{ route('imgareas.edit',$imgarea->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
