@extends('imgareas.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Show Image detail</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('titles.index') }}"> Back</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Text:</strong>
            {{ $imgarea->content }}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Rank:</strong>
            {{ $imgarea->alt }}
        </div>
    </div>
</div>
@endsection
