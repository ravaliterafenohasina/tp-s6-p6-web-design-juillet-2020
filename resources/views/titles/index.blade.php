@extends('titles.layout')

@section('content')
<div class="row" style="margin-bottom: 10px;">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Title </h2>
        </div>
    </div>
</div>

<div class="row" style="margin-bottom: 10px;">
    <div>
        <a class="btn btn-success" href="{{ route('titles.create') }}"> Create New Title</a>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Content</th>
        <th>Description</th>
        <th>Created at </th>
        <th>Updated at </th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($titles as $title)
    <tr>
        <td>{{ $title->id }}</td>
        <td>{{ $title->content }}</td>
        <td>{{ $title->description }}</td>
        <td>{{ $title->created_at }}</td>
        <td>{{ $title->updated_at }}</td>

        <td>
            <form action="{{ route('titles.destroy',$title->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('titles.show',$title->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('titles.edit',$title->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>


@endsection
