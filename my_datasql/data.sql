CREATE USER laravel_user WITH PASSWORD '0333';

/* CREATE ROLE createDb WITH CREATEDB CREATEROLE ; */

GRANT createDb to laravel_user ;

CREATE DATABASE laravel_tutorial ;

ALTER DATABASE laravel_tutorial OWNER TO laravel_user ;

GRANT ALL PRIVILEGES ON DATABASE laravel_tutorial to laravel_user ;

psql -U laravel_user -d laravel_tutorial

/* 1 */ 
INSERT INTO titles(id,content,description,created_at) VALUES('1','Leravel-Tutorial : Tutorial about CRUD',
	'This article will give you example of laravel crud example. This article goes in detailed 
	on laravel tutorial for beginners. Step by step explain how to create crud in laravel .  
	Here, Creating a basic example of laravel  crud application tutorial.','2020-07-06 06:00:00');

/* INSERT INTO titles(id,content,description) VALUES('','',''); */

/* ----------------------------------- H1 ---------------------------------- */

/* Id Parent titles : 1 */
INSERT INTO titleh1s(id,content,rank,idparent,created_at) VALUES('1','Laravel-Tutorial | CRUD in Laravel For Beginners',1,'1','2020-07-06 06:00:00');

/* INSERT INTO titleh1s(id,content,rank,idparent,created_at) VALUES('','',,''); */

/* ----------------------------------- H2 ---------------------------------- */

/* Id Parent Tileh1 : 1 CRUD */
INSERT INTO titleh2s(id,content,rank,idparent,created_at) VALUES('1','Laravel - Database Configuration',1,'1','2020-07-06 06:00:00');
INSERT INTO titleh2s(id,content,rank,idparent,created_at) VALUES('2','Laravel - Create Migration',2,'1','2020-07-06 06:00:00');
INSERT INTO titleh2s(id,content,rank,idparent,created_at) VALUES('3','Laravel - Add Resource Route',3,'1','2020-07-06 06:00:00');
INSERT INTO titleh2s(id,content,rank,idparent,created_at) VALUES('4','Laravel - Add Controller and Model',4,'1','2020-07-06 06:00:00');
INSERT INTO titleh2s(id,content,rank,idparent,created_at) VALUES('5','Laravel - Add Blade Files',5,'1','2020-07-06 06:00:00');

/* ----------------------------------- H3 ---------------------------------- */

/* INSERT INTO Titleh3(id,content,rank,idparent,created_at) VALUES('','',,''); */

/* ----------------------------------- H4 ---------------------------------- */

/* INSERT INTO Titleh4(id,content,rank,idparent,created_at) VALUES('','',,''); */


/* ----------------------------------- Text Area ---------------------------------- */

/* Id Parent titleh2s : 1 */
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('1','We will make database configuration for 
	example database name, username, password etc for our crud application of laravel 7. 
	So let s open .env file and fill all details like as bellow:',1,'1','2020-07-06 06:00:00');


/* ID Parent titleh2s : 2 */
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('2','We are going to create crud 
	application for product. so we have to create migration for `products` table using 
	Laravel 7 php artisan command, so first fire bellow command:',1,'2','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('3','After this command you will 
	find one file in following path `database/migrations` and you have to put bellow code 
	in your migration file for create products table.',2,'2','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('4','Now you have to run this migration by 
	following command:',3,'2','2020-07-06 06:00:00');

/* ID parent titleh2s : 3 */
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('5','Here, we need to add resource route 
	for product crud application. so open your "routes/web.php" file and add following route.',1,'3','2020-07-06 06:00:00');


/* ID parent titleh2s : 4 */
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('6','In this step, now we should create new 
	controller as ProductController. So run bellow command and create new controller. 
	Bellow controller for create resource controller.',1,'4','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('7','After bellow command you will find new 
	file in this path "app/Http/Controllers/ProductController.php".
In this controller will create seven methods by default as bellow methods:',2,'4','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('8','So, let s copy bellow code and put on 
	ProductController.php file.',3,'4','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('9','Ok, so after run bellow command you will 
	find "app/Product.php" and put bellow content in Product.php file:',4,'4','2020-07-06 06:00:00');


/* ID parent titleh2s : 5 */
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('10','In last step. In this step we have to 
	create just blade files. So mainly we have to create layout file and then create new folder 
		"products" then create blade files of crud app. So finally you have to create following 
		bellow blade file:',1,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('11','resources/views/products/layout.blade.php',2,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('12','resources/views/products/index.blade.php',3,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('13','resources/views/products/create.blade.php',4,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('14','resources/views/products/edit.blade.php',5,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('15','resources/views/products/show.blade.php',6,'5','2020-07-06 06:00:00');
INSERT INTO textareas(id,content,rank,idparent,created_at) VALUES('16','Now we are ready to run our crud application example with larave.',7,'5','2020-07-06 06:00:00');




/* ----------------------------------- List Area ---------------------------------- */

/* ID Parent textareas : 7 */
/*
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('1','index()',1,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('2','create()',2,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('3','store()',3,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('4','show()',4,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('5','edit()',5,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('6','update()',6,'7');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('7','destroy()',7,'7');
*/
/* ID Parent textareas : 10 */
/*
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('8','layout.blade.php',1,'10');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('9','index.blade.php',2,'10');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('10','create.blade.php',3,'10');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('11','edit.blade.php',4,'10');
INSERT INTO listareas(id,content,rank,idparent,created_at) VALUES('12','show.blade.php',5,'10');
*/



/* ----------------------------------- Img Area ---------------------------------- */
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('1','db-configuration.png','db-configuration','1','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('2','create-migration-cmd.png','create-migration-cmd','2','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('3','create-migration.png','create-migration','3','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('4','migrate-cmd.png','migrate-cmd','4','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('5','route.png','route','5','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('6','controller-model-cmd.png','controller-model-cmd','6','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('7','controller-model.png','controller-model','8','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('8','product-model.png','product-model','9','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('9','layout-blade-php.png','layout-blade-php','11','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('10','index-blade-php.png','index-blade-php','12','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('11','create-blade-php.png','create-blade-php','13','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('12','edit-blade-php.png','edit-blade-php','14','2020-07-06 06:00:00');
INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('13','show-blade-php.png','show-blade-php','15','2020-07-06 06:00:00');


/* INSERT INTO imgareas(id,content,alt,idparent,created_at) VALUES('','','',''); */
